package ordinogramasRepeticions;

import java.util.Scanner;

public class Repeticions1 {

	public static void main(String[] args) {
		//variables donde se guarda la suma
		int pares = 0;
		int impares = 0;
		
		for (int i = 1; i <= 100; i++) {
			if((i%2) == 0) {//si i modulo 2 es cero, significa que es par
				pares += i;
			}else {//sino es impar
				impares += i;
			}
		}
		System.out.println("Pares en total suman: "+pares);
		System.out.println("Impares en total suman: "+impares);
	}
}
