package ordinogramasRepeticions;

import java.util.Scanner;

public class Repeticions2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//creamos las variables y pedimos un numero
		int inicio = sc.nextInt();
		int fin = sc.nextInt();
		//creamos la variable que guardara la suma total
		int suma = 0;
		
		//iniciamos y acabamos el bucle en los numeros indicados
		for (int i = inicio; i <= fin ; i++) {
			suma+=i;//sumamos cada numero
		}
		
		System.out.println("El total es: "+suma);
	}

}
