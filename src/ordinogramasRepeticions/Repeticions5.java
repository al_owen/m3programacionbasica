package ordinogramasRepeticions;

import java.util.Scanner;

public class Repeticions5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double notas = 0; //notas
		double totalNotas = 0; //suma de las notas
		int numNotas = 0;
		double media = 0;
		
		int excelente = 0;
		int notable = 0;
		int bien = 0;
		int suficiente = 0;
		int insuficiente = 0;
		
		while (notas != -1) {
			notas = sc.nextDouble();
			if (notas >= 0 && notas <= 10) {				
				totalNotas += notas;
				numNotas++;
				if (notas >= 9) {
					excelente++;
				}else if(notas >= 7.5) {
					notable++;
				}else if(notas >= 6) {
					bien++;
				}else if(notas >= 5) {
					suficiente++;
				}else {
					insuficiente++;
				}
			}	
			
		}
		media = totalNotas / numNotas;
		System.out.println("La media es: "+media);
		System.out.println("Hay "+excelente+" excelentes");
		System.out.println("Hay "+notable+" notables");
		System.out.println("Hay "+bien+" bienes");
		System.out.println("Hay "+suficiente+" suficientes");
		System.out.println("Hay "+insuficiente+" insuficientes");
	}
}
