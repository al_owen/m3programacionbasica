package ordinogramasRepeticions;

import java.util.Scanner;

public class Repeticions4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numero = -1; //inicializamos una variable
		
		while(numero != 0){
			numero = sc.nextInt();//pedimos un numero
			if (numero == 0) {
				break;
			}
			if (numero > 0) { //si el numero es positivo
				System.out.println("El numero: "+numero+" es positivo");
			}else {//si el numero es negativo
				System.out.println("El numero: "+numero+" es negativo");	
			}
			
			if (numero % 4 == 0) {//si es divisible entre 4
				System.out.println("El numero: "+numero+" es par y divisible entre 4");
			}else if(numero % 2 == 0) {//si es par
				System.out.println("El numero: "+numero+" es par");
			}else {//si no es ni divisible entre 4 ni par
				System.out.println("El numero: "+numero+" no es par i/o no es divisible entre 4");
			}
			
		}
		
	}
	
}
