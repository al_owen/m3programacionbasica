package ordinogramasRepeticions;

import java.util.Scanner;

public class Repeticions3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numero = 0;//inicializamos la variable
		
		do {
			numero = sc.nextInt(); //pedimos un numero
			System.out.println(numero);
		}while(numero <= 0); //si el numero es mayor a 0 sale del bucle
		
	}
}
