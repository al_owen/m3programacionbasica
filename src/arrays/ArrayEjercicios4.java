package arrays;

import java.util.Random;
import java.util.Scanner;

public class ArrayEjercicios4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		int cantidad = sc.nextInt();
		int[] numeros = new int[cantidad];
			
		
		for (int i = 0; i < cantidad; i++) {
			numeros[i] = r.nextInt(1, 101);
		}
		for (int i = 0; i < numeros.length; i++) {			
			System.out.print(numeros[i]+" ");
		}
		
		int menor = numeros[0];
		int mayor = 0;
		for (int i = 0; i < numeros.length; i++) {
			if (mayor < numeros[i]) {
				mayor = numeros[i];
			}
			if (menor > numeros[i]) {
				menor = numeros[i];
			}
		}
		System.out.println("\nEl menor es: "+menor);
		System.out.println("El mayor es: "+mayor);
		
		
	}
}
