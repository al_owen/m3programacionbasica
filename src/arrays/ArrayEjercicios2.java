package arrays;

import java.util.Random;
import java.util.Scanner;

public class ArrayEjercicios2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		int cantidad = sc.nextInt();
		int[] numeros = new int[cantidad];
		
		for (int i = 0; i < cantidad; i++) {
			numeros[i] = r.nextInt(1, 101);
		}
		for (int i = 0; i < numeros.length; i++) {			
			System.out.print(numeros[i]+" ");
		}
	}
}
