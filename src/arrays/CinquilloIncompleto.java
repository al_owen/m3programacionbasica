package arrays;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class CinquilloIncompleto {

	public static void main(String[] args) {
		Random r = new Random();
		int[][] tapete = new int[4][10];
		String[][] jugadores = new String[4][10];
		String[] baraja = new String[40];
		String[] palos = {"Oros", "Copas", "Espadas", "Bastos"};
		int[] numeros = {1,2,3,4,5,6,7,8,9,10};
		
		//imprimir la matriz del tapete
		for (int i = 0; i < palos.length; i++) {
			System.out.print(palos[i] + " ");
			for (int j = 0; j < tapete[i].length; j++) {
				System.out.print(tapete[i][j]+" ");
			}
			System.out.println();
		}
		//crear baraja
		int contador = 0;
		for (int i = 0; i < palos.length; i++) {
			for (int j = 0; j < numeros.length; j++) {
				String carta = numeros[j]+"-"+palos[i];
				baraja[contador] = carta;
				contador++;
				//System.out.println(carta);
			}
		}
		
		//barajas cartas
		for (int i = 0; i < baraja.length; i++) {
			int indice = r.nextInt(baraja.length);
			String carta = baraja[indice];
			baraja[indice] = baraja[i];
			baraja[i] = carta;
		}
		
		//repartir cartas
		contador = 0;
		for (int i = 0; i < jugadores.length; i++) {
			for (int j = 0; j < jugadores[0].length; j++) {
				jugadores[i][j] = baraja[contador++];
			}
		}
		
		//ver las cartas
		for (int i = 0; i < jugadores.length; i++) {
			for (int j = 0; j < jugadores[i].length; j++) {
				System.out.print(jugadores[i][j]+" ");
			}
			System.out.println();
		}
		
		
		
	}
}
