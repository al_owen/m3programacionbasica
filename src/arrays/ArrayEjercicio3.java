package arrays;

import java.util.Random;
import java.util.Scanner;

public class ArrayEjercicio3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		int cantidad = sc.nextInt();
		int[] numeros = new int[cantidad];
		int[] repetidos = new int[cantidad];
		boolean repetido;
		int numrepetidos = 0;
		
		for (int i = 0; i < cantidad; i++) {
			numeros[i] = r.nextInt(1, 21);
		}
		for (int i = 0; i < numeros.length; i++) {			
			System.out.print(numeros[i]+" ");
		}
		System.out.println();
		for (int i = 0; i < numeros.length; i++) {
			for (int j = i+1; j < numeros.length; j++) {
				if (numeros[i] == numeros[j]) {
					//System.out.println(numeros[i]+" esta duplicado");
					repetido = false;
					for (int k = 0; k < repetidos.length; k++) {
						if (numeros[i] == repetidos[k]) {
							repetido = true;
						}
					}
					
					if(!repetido) {
						repetidos[numrepetidos++] = numeros[i];
						System.out.println("REPETIDO " + numeros[i]);
					}
				}
			}
		}

	}
}
