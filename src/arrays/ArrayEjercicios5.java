package arrays;

import java.util.Random;

public class ArrayEjercicios5 {
	public static void main(String[] args) {
		Random r = new Random();
		int[] array = new int[5];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(1, 21);
		}
		
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
		int temp;
		for (int i = 0; i < array.length; i++) {
			for (int j = i+1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
	}
}
