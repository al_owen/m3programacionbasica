package arrays;

import java.util.Scanner;

public class ArrayEjercicios1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] numeros = new int[5];
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = sc.nextInt();
		}
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.print(numeros[i]+" ");
		}
		System.out.println("\nEscribe 1 si quieres sumar o 2 si quieres multiplicar");
		int opcion = sc.nextInt();
		switch (opcion) {
		case 1:
			int suma = 0;
			for (int i = 0; i < numeros.length; i++) {
				suma += numeros[i];
			}
			System.out.println("El resultado es: "+suma);
			break;
		case 2:
			int multi = 1;
			for (int i = 0; i < numeros.length; i++) {
				multi *= numeros[i];
			}
			System.out.println("El resultado es: "+multi);
			break;
		default:
			break;
		}
	}
}
