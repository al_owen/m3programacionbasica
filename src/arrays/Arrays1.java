package arrays;

public class Arrays1 {
	public static void main(String[] args) {
		String[] diasSemana = new String[7];
		diasSemana[0] = "Lunes";
		diasSemana[1] = "Martes";
		diasSemana[2] = "Miercoles";
		diasSemana[3] = "Jueves";
		diasSemana[4] = "Viernes";
		diasSemana[5] = "Sabado";
		diasSemana[6] = "Domingo";
		
		for (int i = 0; i < diasSemana.length; i++){
			String dia = diasSemana[i];
			System.out.println(dia);
		}
		System.out.println("-------");
		for (String dia : diasSemana) {
			System.out.println(dia);
		}
		
		int[][] numeros = new int[4][3];
		numeros[1][1]= 5;
		System.out.println("----------------");
		for (int[] is : numeros) {
			for (int is2 : is) {
				System.out.print(is2+" ");
			}
			System.out.println();
		}
		int[][][] masNumeros = new int[5][5][5];
		
	}
}
