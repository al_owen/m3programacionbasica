package arrays;

import java.util.Random;
import java.util.Scanner;

public class CinquilloConFunciones {

	static final String[] palos = { "Corazon", "Trebol", "Diamantes", "Picas"};
	static int[][] tapete;
	
	public static void main(String[] args) {
		Random r = new Random();
		Scanner sc = new Scanner(System.in);
		/// variables baraja
		String[] baraja = new String[52];
		
		int[] numeros = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
		int numCartas = 13;

		// variable tapete
		tapete = new int[4][numCartas];
		// inicializar tapete
		System.out.println(inicializar());

		// variables manos
		String[][] jugadores = new String[4][numCartas];
		String[] jugador1 = new String[numCartas];
		String[] jugador2 = new String[numCartas];
		String[] jugador3 = new String[numCartas];
		String[] jugador4 = new String[numCartas];

		// crear baraja
		int indice = 0;
		for (String palo : palos) {
			for (int i = 0; i < numeros.length; i++) {
				String carta = numeros[i] + "-" + palo;
				baraja[indice] = carta;
				indice++;
			}
		}

		// barajar cartas de la baraja
		for (int i = 0; i < baraja.length; i++) {
			int randomIndex = r.nextInt(baraja.length);
			String temp = baraja[randomIndex];
			baraja[randomIndex] = baraja[i];
			baraja[i] = temp;
		}

		// mostrar baraja
//			for (String carta: baraja) {
//				System.out.println(carta);
//			}

		// asignar manos
		indice = 0;
		for (int i = 0; i < numCartas; i++) {
			jugador1[i] = baraja[indice++];
			jugador2[i] = baraja[indice++];
			jugador3[i] = baraja[indice++];
			jugador4[i] = baraja[indice++];
		}
		jugadores[0] = jugador1;
		jugadores[1] = jugador2;
		jugadores[2] = jugador3;
		jugadores[3] = jugador4;

		// mostrar manos
		System.out.println("\n---------------Manos jugadores-----------");
		printMatriz(jugadores);

		System.out.println("\n--------------Logica juego--------------");

		int turno = 0; // variable que determina el turno

		boolean end = false; // flag
		do {
			System.out.println();
			// printa el tapete
			printMatriz(tapete);
			// printa el menu
			menu(turno);
			// pide una opcion
			int option = sc.nextInt();
			switch (option) {
			case 1:
				// printa a los jugadores
				for (int i = 0; i < jugadores[turno].length; i++) {
					System.out.println(i + ".- " + jugadores[turno][i]);
				}
				break;
			case 2:
				// flag
				boolean cartaCorrecta = false;
				// mientras la carta no sea correcta sigue pidiendo carta
				while (!cartaCorrecta) {
					int cartaIndex = sc.nextInt();// pide el indice
					// si el indice indicado se sale de los limites
					if (cartaIndex >= numCartas || cartaIndex < 0) {
						System.out.println("\nError de posicion");
						break;
					}
					// guardamos la carta
					String carta = jugadores[turno][cartaIndex];
					if (!carta.equals(" ")) {// verificamos que haya una carta
						String[] splitCard = carta.split("-");// separamos su valor en el numero y palo
						int num = Integer.valueOf(splitCard[0]);// convertimos el numero en integer
						String palo = splitCard[1];// guardamos el palo

						for (int i = 0; i < palos.length; i++) {// recorremos los palos
							if (palo.equals(palos[i])) {// si el palo coincide sabemos el indice
								if (tapete[i][num - 1] == 0) { // si la posicion esta vacia
									if (num == 5) {// si el numero es 5 lo ponemos en su sitio
										tapete[i][num - 1] = num;// ponemos el 5 en su sitio correspondiente
										jugadores[turno][cartaIndex] = " ";// borramos la carta de la mano del jugador
										cartaCorrecta = true;// decimos que se ha dado una carta correcta
									} else if (tapete[i][num] > num && tapete[i][num] != 0) {// verificamos si la
																								// posicion siguiente
																								// tiene algo y el
																								// numero es menor
										tapete[i][num - 1] = num; // lo ponemos en la posicion correspondiente
										jugadores[turno][cartaIndex] = " ";// borramos la carta de la mano del jugador
										cartaCorrecta = true;// decimos que se ha dado una carta correcta
									} else if (tapete[i][num - 2] < num && tapete[i][num - 2] != 0) {// verificamos si
																										// la posicion
																										// anterior
																										// tiene algo y
																										// el numero es
																										// mayor
										tapete[i][num - 1] = num;// lo ponemos en la posicion correspondiente
										jugadores[turno][cartaIndex] = " ";// borramos la carta de la mano del jugador
										cartaCorrecta = true;// decimos que se ha dado una carta correcta
									}
								}
							}
						}
						// comprobamos si algun jugador ha ganado
						for (int i = 0; i < jugadores.length; i++) {
							int cartasEnMano = 0;
							for (int k = 0; k < jugadores[i].length; k++) {
								if (jugadores[i][k].equals(" ")) {
									cartasEnMano++;
									if (cartasEnMano == numCartas) {
										System.out.println("Felicidades! El jugador " + turno + " ha ganado");
										end = true;
									}
								}

							}
						}
						// pasamos turno
						turno++;
						turno = (turno >= 4) ? 0 : turno;// si el turno se pasa de 3 lo reseteamos
					}
				}

				break;
			case 3:
				turno++;// pasamos turno
				turno = (turno >= 4) ? 0 : turno;// si el turno se pasa de 3 lo reseteamos
				break;
			case 4:
				end = true;// acabamos el turno
				break;
			default:
				System.out.println("Opcion invalida");
				break;
			}

		} while (!end);
		sc.close();
	}
	
	private static void menu(int pepe) {
		System.out.println("\n-------Juego-------\n");
		System.out.println("1. Mostrar mano");
		System.out.println("2. Elegir carta");
		System.out.println("3. pasar");
		System.out.println("4. Salir de juego");
		System.out.println("\n turno jugador " + pepe);
		
	}

	private static void printMatriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			//System.out.print(palos[i] + " ");
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	private static void printMatriz(String[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			//System.out.print(palos[i] + " ");
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		
	}

	public static int[][] inicializar() {
		System.out.println("Tapete");
		for (int i = 0; i < tapete.length; i++) {
			System.out.print(palos[i] + " ");
			for (int j = 0; j < tapete[0].length; j++) {
				tapete[i][j] = 0;
				System.out.print(tapete[i][j] + " ");
			}
			System.out.println();
		}
		return tapete;
	}
	
}
