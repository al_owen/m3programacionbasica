package ordinogramasCondicionals;

import java.util.Scanner;

public class Condicionales2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		//creamos variables para comprobar
		//si el numero modulo 2 y modulo 3 tiene residuo
		int multi2= numero % 2;
		int multi3= numero % 3;
		
		//comprobamos que es multiple de 2 o de 3
		if(multi2 == 0 || multi3 == 0){
			//si es multilpe de dos
			if(multi2 == 0) {
				System.out.println("Numero es multiple de 2");
			}
			//si es multiple de tres
			if (multi3 == 0) {
				System.out.println("Numero es multiple de 3");
			}
			//si no es multiple ni de 2 ni de 3
		}else {
			System.out.println("No es multiple de 2 ni de 3");
		}
		
	}

}
