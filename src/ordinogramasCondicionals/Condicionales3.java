package ordinogramasCondicionals;

import java.util.Scanner;

public class Condicionales3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//pedimos un precio
		System.out.println("Precio: ");
		int precio = sc.nextInt();
		//pedimos una cantidad
		System.out.println("Cantidad: ");
		int cantidad = sc.nextInt();
		//calculamos el precio total
		int precioTotal = precio * cantidad;
		
		//si el precio total es mayor a 1000
		if (precioTotal >= 1000) {
			//se aplica un 40% de descuento, 
			//es decir, el precio con descuento es el 60% del precio total
			System.out.println("El precio final es: "+(precioTotal*0.6)+" se aplico el 40% de descuento");
		//si el precio total es mayor a 500
		}else if (precioTotal >= 500) {
			//se aplica un 20% de descuento, 
			//es decir, el precio con descuento es el 80% del precio total
			System.out.println("El precio final es: "+(precioTotal*0.8)+" se aplico el 20% de descuento");
		}
		

	}

}
