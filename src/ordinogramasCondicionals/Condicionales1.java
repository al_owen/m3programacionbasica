package ordinogramasCondicionals;

import java.util.Scanner;

public class Condicionales1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//pedimos un numero
		int nombre = sc.nextInt();
		//verificamos si el numero es mayor, menor o igual a 10
		if (nombre > 10) {
			System.out.println("El numero es mayor que 10");
		}else if (nombre < 10) {
			System.out.println("El numero es menor que 10");
		}else if (nombre == 10) {
			System.out.println("El numero es igual a 10");
		}
		
		
	}

}
