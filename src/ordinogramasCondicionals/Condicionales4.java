package ordinogramasCondicionals;

import java.util.Scanner;

public class Condicionales4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//Pedimos nombre y edad 3 veces
		
		System.out.println("Escribe el nombre");
		String nom1 = sc.nextLine();
		System.out.println("EScribe la edad");
		int edad1 = sc.nextInt();
		
		System.out.println("Escribe el nombre");
		String nom2 = sc.nextLine();
		System.out.println("EScribe la edad");
		int edad2 = sc.nextInt();
		
		System.out.println("Escribe el nombre");
		String nom3 = sc.nextLine();
		System.out.println("EScribe la edad");
		int edad3 = sc.nextInt();

		//comparamos quien es mayor, el mediano y el menor
		if (edad1 > edad2 && edad2 > edad3) {
			System.out.println("El mayor es "+edad1);
			System.out.println("El mediano es "+edad2);
			System.out.println("El menor es "+edad3);
		}else if(edad1 > edad2 && edad2 < edad3) {
			System.out.println("El mayor es "+edad1);
			System.out.println("El mediano es "+edad3);
			System.out.println("El menor es "+edad2);
		}else if(edad1 < edad2 && edad1 > edad3) {
			System.out.println("El mayor es "+edad2);
			System.out.println("El mediano es "+edad1);
			System.out.println("El menor es "+edad3);
		}else if(edad2 < edad3 && edad3 > edad1) {
			System.out.println("El mayor es "+edad3);
			System.out.println("El mediano es "+edad2);
			System.out.println("El menor es "+edad1);
		}
		
	}

}
