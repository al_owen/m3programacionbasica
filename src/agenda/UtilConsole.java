package agenda;

import java.util.Scanner;
import java.util.Random;

public class UtilConsole {

	private static Scanner sc;
	private static Random r;
	//"^(.+)@(.+)$"
	
	/**
	 * inicializa el scanner y el random
	 * @param scanner si queremos que cree un scanner
	 * @param random si queremos que cree un random
	 */
	public static void init(boolean scanner, boolean random) {
		if (sc == null) {
			sc = new Scanner(System.in);			
		}
		if (random == true) {
			r = new Random();
		}
	}
	
	public static void initRandom() {
		if (sc == null) {
			sc = new Scanner(System.in);			
		}
		if (r == null) {
			r = new Random();
		}
	}
	
	public static void Close() {
		sc.close();
	}
	
	public static int pedirInt() {
		String text ="";
		int num = 0;
		do {
			text = sc.nextLine();
			if (text.matches("[0-9]+")) {
				num = Integer.parseInt(text);
			}else {
				System.out.println("El valor es incorrecto");
			}
		} while (!text.matches("[0-9]+"));
		
		return num;
	}
	
	public static String pedirString() {
		String text = sc.nextLine();
		return text;
	}
	
	
	
}
