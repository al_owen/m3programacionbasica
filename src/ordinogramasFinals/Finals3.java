package ordinogramasFinals;

import java.util.Scanner;

public class Finals3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean seguir = true;
		int num1, num2;
		
		while (seguir) {
			System.out.println("\n0. Sortir ");
			System.out.println("1. Suma");
			System.out.println("2. Resta");
			System.out.println("3. Multiplicación");
			System.out.println("4. División");
			int opcion = sc.nextInt();
			switch (opcion) {
			case 0:
				seguir = false;
				break;
			case 1:
				num1 = sc.nextInt();
				num2 = sc.nextInt();
				System.out.println("El resultado es: "+(num1+num2));
				break;
			case 2:
				num1 = sc.nextInt();
				num2 = sc.nextInt();
				System.out.println("El resultado es: "+(num1-num2));
				break;
			case 3:
				num1 = sc.nextInt();
				num2 = sc.nextInt();
				System.out.println("El resultado es: "+(num1*num2));
				break;
			case 4:
				num1 = sc.nextInt();
				do {
					num2 = sc.nextInt();
					if(num2 == 0) {
						System.out.println("No se puede dividir entre 0!!!!!!!!");
					}
				} while (num2 == 0);
				System.out.println("El resultado es: "+(num1/num2));
				break;
			default:
				System.out.println("Opcion no valida");
				break;
			}
		}
		
	}
}
