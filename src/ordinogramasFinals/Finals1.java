package ordinogramasFinals;

import java.util.Scanner;

public class Finals1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int limit = 0;
		int numeros = 0;
		
		do {
			System.out.println("Introduce el limite");
			limit = sc.nextInt();
		}while(limit<0);
		
		while (numeros<=limit) {
			System.out.println("Introduce el numero");
			numeros += sc.nextInt();
			System.out.println("la suma es: "+numeros);
		}
		System.out.println("Has superado el limite!");
		
		
	}
}
