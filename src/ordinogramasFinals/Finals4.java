package ordinogramasFinals;

import java.util.Random;
import java.util.Scanner;

public class Finals4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		int numAleatorio = r.nextInt(0, 100);
		int num = 0;
		
		boolean seguir = true;
		while (seguir) {
			num = sc.nextInt();
			if (num > numAleatorio) {
				System.out.println("El numero es más pequeño");
			}else if (num < numAleatorio) {
				System.out.println("El numero es más grande");
			}else {
				System.out.println("Acertaste!!!!!!");
				seguir = false;
			}
		}
		
	}
}
