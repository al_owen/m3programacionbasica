package funciones;

public class Persona {

	private static int PersonasCreadas;
	
	private String nombre;
	private int edad;
	
	public Persona() {
		PersonasCreadas++;
	}
	
	public static int verPersonasCreadas() {
		//edad = 18;
		return PersonasCreadas;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	
}
