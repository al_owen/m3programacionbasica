package funciones;

import java.util.ArrayList;
import java.util.Scanner;

public class FuncionesPruebas {

	static Scanner sc;
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		ArrayList<String> numeros = new ArrayList<>();
		mensaje();
		//System.out.println(suma(7, 8));
		
	}
	
	/**
	 * Esta funcion sirve para sumar dos parametros
	 * @param num1
	 * @param num2
	 * @return
	 */
	public static int suma(int num1, int num2) {
		int resultado = num1 + num2;
		return resultado;
	}
	
	public static void mensaje() {
		System.out.println("Hola usuario");
		System.out.println("El resultado es: "+suma(7,9));
	}
	
}
