package condicionales;

import java.util.Locale;
import java.util.Scanner;

public class Switch_ejemplo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		System.out.println("Escribe un dia de la semana en numero");
		int diaSemana = sc.nextInt();
		
		switch (diaSemana) {
		case 1:
			System.out.println("Es Lunes");
			break;
		case 2:
			System.out.println("Es Martes");
			break;
		case 3:
			System.out.println("Es Miercoles");
			break;
		case 4:
			System.out.println("Es Jueves");
			break;
		case 5:
			System.out.println("Es Viernes");
			break;
		case 6:
			System.out.println("Es Sabado");
			break;
		case 7:
			System.out.println("Es Domingo");
			break;
		default:
			System.out.println("No existe ese día");
			break;
		}
		
	}
}
