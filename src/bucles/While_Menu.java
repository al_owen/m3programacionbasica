package bucles;

import java.util.Scanner;

public class While_Menu {
	public static void main(String[] args) {
		Scanner sc =  new Scanner(System.in);
		//declaramos la variable
		String name = "user";
		//declaramos el final
		boolean finish = false;
		
		//iniciamos el bucle
		while (finish == false) {
			System.out.println("Escoge una opcion");
			System.out.println("Opcion 1: escribe tu nombre");
			System.out.println("Opcion 2: imprime tu nombre");
			System.out.println("Opcion 3: Salir");
			//pedimos una opcion
			int option = sc.nextInt();
			sc.nextLine(); //quitamos el salto de linea
			//comparamos las opciones
			switch (option) {
			case 1: 
				name = sc.nextLine();
				break;
			case 2:
				System.out.println("Hola, "+name);
				break;
			case 3:
				finish = false;
				break;
			default:
				System.out.println("Opcion invalida, vuelve a probar!");
				break;
			}
		}
	}
}
