package bucles;

import java.util.Scanner;

public class Do_while_ejemplo {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//creamos la condicion para salir del menu
		boolean finish = true;
		//inicializamos la variable que guarda el nombre
		String name = "";
		
		do {
			System.out.println("Escoge una opcion");
			System.out.println("Opcion 1: escribe tu nombre");
			System.out.println("Opcion 2: imprime tu nombre");
			System.out.println("Opcion 3: Salir");
			int option = sc.nextInt();//pedimos un numero
			sc.nextLine();//quitamos el salto de linea que se queda al pedir un numero
			//comparamos los numeros
			switch (option) {
			case 1: //opcion 1
				name = sc.nextLine();
				break;
			case 2: //opcion2
				System.out.println("Hola, "+name);
				break;
			case 3: //opcion3
				finish = true;
				break;
			default: //cualquier otro numero
				System.out.println("Opcion invalida, vuelve a probar!");
				break;
			}
		}while (finish == false);
	}
}
