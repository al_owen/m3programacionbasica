package bucles;

public class For_ejemplo {
	public static void main(String[] args) {
		/*
		for (int i = 100; //inicio
				i >= 1; //condicion para seguir en el bucle
				i--) { //como llega del inicio al final
			*/
		for (int i = 100; i >=1; i--) {
			if (i % 2 == 0) {
				System.out.println(i+" es par");
			}else {
				System.out.println(i+" es impar");
			}
		}
	}
}
