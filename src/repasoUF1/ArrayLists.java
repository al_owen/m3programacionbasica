package repasoUF1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class ArrayLists {

	public static void main(String[] args) {
		
		/*****Crear ArrayLists*****/
		
		//creacion de una arrayList
		ArrayList<String> listaCompra = new ArrayList<>();
		//creacion de una lista con valores ya establecidos
		ArrayList<Integer> listaNumeros = new ArrayList<>(Arrays.asList(111,2,55,43,3));
		
		// *****Ejemplos de uso*****//
		
		// añadir un elemento nuevo
		listaCompra.add("patata");
		listaCompra.add("pan");
		
		// ver el tamaño de la lista
		System.out.println(listaCompra.size());
		
		// imprimir la lista
		System.out.println(listaCompra);
		
		// obtener el objeto en el indice indicado
		System.out.println(listaCompra.get(0));
		
		// remplazar el objeto en el indice indicado
		listaCompra.set(1, "leche"); 
		
		// añadir un elemento en el indice indicado
		listaCompra.add(1, "leche"); 
		System.out.println(listaCompra);
		
		// borrar el elemento en el indice indicado
		listaCompra.remove(1);
		System.out.println(listaCompra);
		
		// convertir a array la arraylist
		Object[] listaAcabada = listaCompra.toArray();
		for (int i = 0; i < listaAcabada.length; i++) {
			System.out.print(listaAcabada[i] + " ");
		}
		System.out.println();
		
		// ver el indice del elemento indicado
		System.out.println(listaCompra.indexOf(""));
		
		// ordenar la lista
		Collections.sort(listaCompra);
		System.out.println(listaCompra);
		
		 // desordenar la lista
		Collections.shuffle(listaCompra);
		System.out.println(listaCompra);
		
		// mostrar la lista invertida
		Collections.reverse(listaCompra);
		System.out.println(listaCompra);
	}

}
