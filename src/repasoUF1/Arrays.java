package repasoUF1;

import java.util.Random;

import pokemon.Pokemon;
/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class Arrays {

	/*
	 * Los arrays son grupos de valores. El tamaño de las arrays no se puede
	 * modificar una vez se haya creado. Las posiciones empiezan siempre por 0, es
	 * decir, la primera posicion es 0, la segunda 1, la tercera 2, etc..
	 */

	public static void main(String[] args) {
		// *****Creacion de arrays*****//

		// ***** creacion de una array vacia
		int[] numeros = new int[5];// es obligatorio especificar el tamaño de la array
		
		// ***** creacion de una array con valores
		String[] pokemons = { "Pockachu", "Dikorita", "Karmander", "Mortres" };

		// Cuando creamos una Array de objetos, por defecto, lo llenara de NULLs. Por
		// ejemplo:
		Pokemon[] pk = new Pokemon[2];// la array será [NULL, NULL]
		
		// en el caso de los primitivos dependerá del tipo, por ejemplo:
		// si es una array de:
		// int será 0 --> [0,0,0,0]
		// char será '' --> ['','','']
		// boolean será false --> [false, false]

		// *****Ejemplos de uso*****//

		// ***** "Rellenar" una array
		// para acceder a una posicion se escribe entre corchetes la posicion de la
		// array que queremos ver o modificar

		// para modificarlo solo hace falta acceder a la posicion y decirle que es igual
		// a algo
		numeros[1] = 3;
		// para pedir el valor por consola y guardarlo solo hace falta recorrer cada
		// posicion y pedir un valor
		for (int i = 0; i < numeros.length; i++) {
			// numeros[i] = sc.nextInt(); <-- esto pediria por un scanner el valor y lo
			// guardaria en la posicion que toque
			numeros[i] = new Random().nextInt(100);// esto lo llena de manera aleatoria
		}

		// *****Mostrar los valores
		// simplemente escribiendo la array[posicion] nos devolverá el valor de la
		// posición
		System.out.println(pokemons[1]);
		// si queremos mostrar todos los valores de una array debemos recorrerla y
		// mostrar uno por uno
		for (int i = 0; i < pokemons.length; i++) {// pokemons.lenght nos devuelve el tamaño de la array de pokemons, en
													// este caso 4
			System.out.print(pokemons[i] + " ");// utilizamos System.out.print envés de println porque así lo imprimira
												// en una sola linea
		}
		System.out.println();// esto solo sirve para hacer un salto de linea, para que el siguiente syso este
								// debajo y no al lado

	}
}
