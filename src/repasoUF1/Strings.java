package repasoUF1;

import java.util.Iterator;

/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class Strings {

	/*
	 * Las Strings son cadenas de caracteres que no se pueden modificar
	 * es decir, cada vez que le concatenamos algo, reemplazamos, modificamos, 
	 * eliminamos, etc... lo que realmente hacemos es crear una nueva cadena que debemos guardar 
	 * */
	
	public static void main(String[] args) {
		
		/*****Crear String*****/
		
		//se puede crear una String como cualquier objeto, llamando al constructor
		String st = new String();
		System.out.println(st);//nos aparecera un espacio vacio porque la cadena esta creada pero vacia
		
		//Tambien podemos simplemente crearlo utitlizando comillas dobles -> ""
		String text = "Hello";
		System.out.println(text);
		
		// *****Ejemplos de uso*****//
		
		//fijaros que si utilizo la funcion concat no modifica el texto
		st.concat("Hola que tal");
		System.out.println(st);//nos aparecera un espacio vacio porque la cadena esta creada pero vacia
		
		//sin embargo si lo guardo dentro de otra variable se verá el cambio
		String st2 = st.concat("Hola que tal");
		System.out.println(st2);
		
		//una solución fácil para no tener que crear muchas variables es reutilizar la misma variable
		//es decir nombreVariable = nombreVariable.concat("text");
		System.out.println("texto antes de concatenar: "+text);//texto antes de concatenar 
		text = text.concat(" darkness");
		System.out.println("texto despues de concatenar: "+ text);//texto despues de concatenar
		
		//de la misma manera se puede concatenar utilizando el simbolo '+' en ves de la funcion concat
		text = text + " my old friend";
		System.out.println("concatenado con el simbolo '+': "+text);
		
		//mostrar el caracter en la posición indicada
		System.out.println("Primer caracter: "+text.charAt(0));
		
		//creamos una variable para comparar
		String text2 = "Hello darkness my old friend";
		//comapara 2 StringBuilders y nos devuelve 0 si son iguales y -1 si son diferentes
		System.out.println("el text es igual al text2 "+text.compareTo(text2));
				
		//creamos una variable para comparar
		String text3 = "HELLO dArkneSS My Old frIend";
		//verifica que el contenido lexico de text sea igual al text2, tiene en cuenta las mayusculas y minusculas
		System.out.println("el text es igual al text2 "+text.equals(text2));
		System.out.println("el text es igual al text3 "+text.equals(text3));

		//verifica que el contenido lexico de text sea igual al text2 sin tener en cuenta las mayusculas y minusculas
		System.out.println("el text es igual al text3 aun si no coinciden las mayusculas y minusculas? "+text.equalsIgnoreCase(text3));

		//verifica si nuestra cadena contiene la cadena indicada
		System.out.println("La cadena contiene la palabra 'darkness'? "+text.contains("darkness"));
		
		String textoRepetido = "Lorem Ipsum, Lorem Ipsum, Lorem Ipsum";
		//nos devuelve la primera posicion inicial de la cadena indicada si la encuentra, sino nos devuelve -1
		System.out.println("La posicion de la primera palabra 'Lorem' es: "+textoRepetido.indexOf("Lorem"));
		//nos devuelve la ultima posicion ionicial de la cadena indicada si la encuentra, sino nos devuelve -1
		System.out.println("La posicion de la ultima palabra 'Lorem' es: "+textoRepetido.lastIndexOf("Lorem"));

		//verifica si el texto esta en blanco
		String textoEnBlanco = "         ";
		System.out.println("El texto esta en blanco? "+textoEnBlanco.isBlank());
		
		//verifica si el texto esta vacio
		System.out.println("El texto esta vacio? "+textoEnBlanco.isEmpty());
		
		//nos devuelve el largo de la palabra
		System.out.println("El text tiene "+text.length()+" caracteres");
		
		//repite la string varias veces
		System.out.println(text.repeat(3));
		
		//remplaza cada vez que encuentre una cadena que coincida con la cadena indicada
		System.out.println("Texto: "+textoRepetido);
		System.out.println("Texto remplazado: "+textoRepetido.replaceAll("Lorem", "dolor"));
		
		//elimina los espacios al principio y al final del texto
		String textoConEspacios = "      Texto con spacios        ";
		System.out.println("inicio-"+textoConEspacios+"-final");
		System.out.println("inicio-"+textoConEspacios.strip()+"-final");
		//elimina los espacios al principio del texto
		System.out.println("inicio-"+textoConEspacios.stripLeading()+"-final");
		//elimina los espacios al final del texto
		System.out.println("inicio-"+textoConEspacios.stripTrailing()+"-final");
		
		//podemos cojer una parte de la cadena de texto, indicando la posición inicial(inclusivo)y el final (exclusivo)
		//nos lo devuelve en forma de String
		System.out.println(text.substring(6, 14));
		
		//si no ponemos la posición final nos devuelve la cadena desde la posición inicial hasta el final
		System.out.println(text.substring(15));
		
		//nos devuelve la string en mayusculas
		System.out.println(text.toUpperCase());
		
		//nos devuelve la string en minusculas
		System.out.println(text.toLowerCase());
		
		//guardar en una array cada elemento de la string separado por el elemento indicado
		String noms = "Alvaro,Andres,Pol,Jaume,Judith,Christian,Marta,Harold,Hilberto,Ferran,Eudald";
		String[] nombres = noms.split(",");
		for (int i = 0; i < nombres.length; i++) {
			System.out.println(nombres[i]);
		}
		
	}
	
}
