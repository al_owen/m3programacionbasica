package repasoUF1;

/**
 * 
 * @author Álvaro Owen de la Quintana 18 nov 2022
 */
public class StringBuilders {

	/*
	 * Las string builders son cadenas de caracteres similares a String
	 * pero con una diferencia importante. Los stringBuilders son modificables,
	 * es decir, podemos modificar la cadena de texto, concatenarle texto,
	 * revertir la cadena de texto, eliminar letras, etc. 
	 * */
	
	public static void main(String[] args) {
		
		/*****Crear StringBuilders*****/
		
		//Se puede crear una StringBuilder vacia
		StringBuilder sb = new StringBuilder();
		//podemos crear una StringBuilder con un valor inicial
		StringBuilder text = new StringBuilder("Pokachus es el mejor");
		StringBuilder text2 = new StringBuilder("Pokachu el mejor");
		
		//le agregamos una cadenas de texto
		sb.append("Hola ");
		//se le puede pasar una array de chars y lo agrega letra a letra
		char[] var = {'a','d','i','o','s',' '};
		sb.append(var);
		//se puede agregar valores de tipo numerico y lo convierte automaticamente
		sb.append(5);
		//incluso se puede agregar un boolean
		sb.append(false);
		System.out.println(sb);//mostramos el valor
		
		// *****Ejemplos de uso*****//
		
		//mostramos la capacidad de la StringBuilder, se muestra la capacidad que tiene,
		//no la cantidad de caracteres escritos 
		System.out.println("la capacidad es: "+text.capacity());
		
		//muestra la cantidad de caracteres escritos 
		System.out.println("tiene "+text.length()+ " letras");
		
		//muestra el caracter escrito en la posicion especificada(cada letra es una posición)
		System.out.println("el primer caracter es: "+text.charAt(0));
		
		//elimina la sequencia de caracteres desde la posicion indicada, hasta la otra posición indicada.
		//en este caso eliminamos la palabra "es", de la frase "Pokachu es el mejor", 
		//"es" inicia en la posición 8 y acaba en la posición 10 
		text.delete(8, 11);//las posiciones son el inicio (inclusivo), el final (exclusivo) 
		System.out.println(text);
		
		//indexOf indica la posición en la que inicia la palabra indicada, devuelve -1 si no la encuentra
		System.out.println("la posicion inicial de la palabra 'el' es: "+text.indexOf("el"));
		
		//borra el caracter en la posición 7, en este caso la 's' de pokachaus 
		text.deleteCharAt(7);
		System.out.println(text);

		//comapara 2 StringBuilders y nos devuelve 0 si son iguales y -1 si son diferentes
		System.out.println("el text es igual al text2 "+text.compareTo(text2));
		
		//nos permite insertar un elemento de cualquier tipo; int, double, float, object, boolean, etc...
		//debemos indicar la posicion donde queremos insertar el elemento
		//no remplaza la posicion sino que recorre el resto de la cadena a la derecha 
		text.insert(7,",");
		System.out.println(text);

		//creamos una StringBuilder vacia
		StringBuilder sbVacia = new StringBuilder();
		//verificamos si la string está vacia nos devuelve true si está vacia y false si no.
		System.out.println(sbVacia.isEmpty());
		
		//podemos remplazar palabras por ejemplo pokachu por dikorita 
		//debemos indicar el inicio de la cadena a remplazar (inclusivo), el final (exclusivo) y la nueva cadena 
		text.replace(0, 7, "Dikorita");
		System.out.println(text);
		
		//invierte la cadena 
		text.reverse();
		System.out.println(text);
		
		//podemos cojer una parte de la cadena de texto, indicando la posición inicial(inclusivo)y el final (exclusivo)
		//nos lo devuelve en forma de String, por lo tanto lo debemos guardar en una variable de este tipo
		String substring = text.substring(0, 5);
		System.out.println(substring);

		//si no ponemos la posición final nos devuelve la cadena desde la posición inicial hasta el final
		String substring2 = text.substring(10);
		System.out.println(substring2);
		
		//podemos acortar la cadena indicando el largo maximo.
		sb.setLength(10);
		System.out.println(sb);

		//si solo quisieramos quitar la ultima letra. podriamos indicar el largo menos uno
		sb.setLength(sb.length()-1);
		System.out.println(sb);
	}

}
