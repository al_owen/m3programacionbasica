package repasoUF1;

import java.util.Random;
/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class Matrices {
	/*
	 * Las matrices son arrays de arrays --> [[0,1,2],[4,5,6],[7,8,9]]. Normalmente
	 * las representamos de manera vertical, cada array dentro de la array
	 * representa una fila y cada elemento de cada array representa una columna
	 */

	public static void main(String[] args) {
		// *****Creacion de Matrices*****//

		//Creación de una matriz "Vacia"
		int[][] numeros = new int[3][3];

		//Creación de una matriz con valores predefinidos
		String[][] matriz = { { "?", "?", "?" }, { "?", "?", "?" } };

		
		// *****Ejemplos de uso*****//

		// ***** "Rellenar" una matriz
		// para acceder a una posicion se escribe entre corchetes la posicion en la fila
		// y en la columna de la matriz que queremos ver o modificar

		matriz[0][1] = "hola";
		// esto modificará solo esa coordenada,
		// es decir --> [["?", "hola", "?"]["?","?", "?"]]

		// *****Recorrer todo las posiciones
		// para pedir el valor por consola y guardarlo solo hace falta recorrer cada
		// posicion y pedir un valor
		for (int fila = 0; fila < numeros.length; fila++) {// primero nos recorremos las filas
			for (int columna = 0; columna < numeros[0].length; columna++) {// por cada array de fila nos recorremos sus
																			// columnas
				// numeros[fila][columna] = sc.nextInt(); <-- esto pediria por un scanner el
				// valor y lo
				// guardaria en la posicion que toque
				numeros[fila][columna] = new Random().nextInt(100);// esto lo llena de manera aleatoria
			}
		}

		// *****Mostrar los valores de la matriz
		// simplemente escribiendo la matriz[fila][columna] nos devolverá el valor de
		// la posición
		System.out.println(matriz[0][1]);
		// si queremos mostrar todos los valores de una matriz debemos recorrerla y
		// mostrar uno por uno
		for (int fila = 0; fila < numeros.length; fila++) {// primero nos recorremos las filas
			// por cada array de fila nos recorremos sus columnas
			for (int columna = 0; columna < numeros[0].length; columna++) {
				System.out.print(numeros[fila][columna]+" ");// esto nos muestra cada posicion
				//se utiliza la funcion print envés de println porque así nos muestra toda las filas en una sola fila
			}
			System.out.println();//hacemos un salto de linea para que la siguiente fila se muestre debajo
		}
		
	}
}
