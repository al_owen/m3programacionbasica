package repasoUF1;

/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class Funciones {

	/* Las funciones, también llamados métodos, nos permiten dividir el trabajo que hace un programa, 
	 * en tareas más pequeñas separadas de la parte principal.
	 * */
	
	private static int suma; //creamos una variable static 
	private String name;
	
	public static void main(String[] args) {
		//llamamos a la funcion sin retorno
		saludarUsuario("Alvaro");
		
		//llamamos a la funcion con retorno
		int res = sumar(5, 13);
		System.out.println("El resultado es: "+res);
		
		//creamos un objeto para acceder a las funciones del objeto "Funciones"
		Funciones f = new Funciones(); //podemos crear un objeto de cualquier clase
		f.cambiarNombre("Alvaro");
		System.out.println(f.verNombre());
		f.saludarUser();
		
	}
	
	/**
	 * La declaracion de la funcion se divide en varias partes.
	 * 1. Los modificadores, indican la visibilidad -> public, private, protected, static
	 * 2. Tipo de retorno, indica el tipo de valor que se devolverá, en caso que no se devuelva nada se debe indicar void
	 * 3. Nombre de la funcion, mediante la cual la invocaremos
	 * 4. Argumentos, variables locales que indican el tipo de valores que nos tienen que pasar al invocar ea la funcion  
	 */
	
	//public -> la funcion será visible fuera de esta clase
	//private -> la funcion solo es visible dentro de esta clase
	//protected -> la funcion será visible fuera de esta clase pero solo si esta dentro del mismo paquete
	//static -> No hace falta crear un objeto para acceder a esta funcion, se invoca mediante la clase no el objeto. Solo puede invocar a otros statics
	
	//*******************Ejemplos********************
	
	//funcion static, que no devuelve nada
	/**
	 * Metodo que pide un nombre y le saluda
	 * @param nombre de Usuario
	 */
	public static void saludarUsuario(String nomUser) {
		//al estar en una funcion static no podemos usar variables no static
		//System.out.println("Hola "+name); //si descomentas esta linea veras que se queja
		System.out.println("Hola "+nomUser);
	}
	
	//funcion static que devuelve un numero
	/**
	 * Metodo que suma 2 numeros 
	 * @param num1
	 * @param num2
	 * @return la suma de ambos numeros
	 */
	public static int sumar(int num1, int num2) {
		//al ser la variable static la podemos modificar desde cualquier parte del codigo
		suma = num1 + num2;
		return suma;
	}
	
	//funcion no static que pide una String pero no devuelve nada
	/**
	 * Metodo que modifica el nombre
	 * @param newName
	 */
	public void cambiarNombre(String newName) {
		name = newName;
	}

	//Funcion no static que devuelve una string
	/**
	 * Metodo que devuelve el nombre
	 * @return el nombre del objeto
	 */
	public String verNombre() {
		return name;
	}
	
	
	//funcion no static que no devuelve nada
	/**
	 * Metodo que saluda al usuario
	 */
	public void saludarUser() {
		//podemos llamar a la variable no static porque esta funcion no es static
		System.out.println("Hello "+name);
	}
}
