package repasoUF1;

/**
 * 
 * @author Álvaro Owen de la Quintana
 * 18 nov 2022
 */
public class Variables {

	/* 
	 * Una variable es donde se almacenan y se recuperan los datos de un programa.
	 * En programación, la utilizamos para guardar datos y estados,asignar ciertos valores de variables a otras, 
	 * representar valores de expresiones matemáticas y mostrar valores por pantallas.  
	 */
	public static void main(String[] args) {
		/*
		 * las variables se declaran de la siguiente manera: Tipo nombre
		 * si queremos asignarle un valor se hace mediante: = valor
		 */
		
		//****************Declaracion de variables************
		//sin valor
		int num;
		String nom;
		double dec;
		boolean bool;
		
		//con valores de inicio
		int numero = 10;
		String nombre = "Alvaro"; 
		double decimal = 5.8;
		boolean booleano = true;
		
		//****************Modificar los valores*******************
		num = 8;
		nom = "Pokachu";
		dec = 66.6;
		bool = false;
	}
}
