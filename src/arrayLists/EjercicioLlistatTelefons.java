package arrayLists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class EjercicioLlistatTelefons {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<String> numeros = new ArrayList<>();
		//añadimos manualmente los telefonos uno por uno
		numeros.add("712345678");
		numeros.add("612345678");
		numeros.add("687654321");
		numeros.add("787654321");
		//tambien podemos añadir varios objetos a la vez con addAll
		numeros.addAll(Arrays.asList("787554227","722575978"));
		
		//en el momento de crearlo tambien le puedes pasar una lista inicial con Arrays.asList()
		ArrayList<String> numeros2 = new ArrayList<>(Arrays.asList("787654321","712345678"));
		String numero;
		int op = 0;
		do {
			System.out.println("0. Sortir");
			System.out.println("1. Inserir un nou telèfon a la llista.");
			System.out.println("2. Imprimir la llista ordenada.");
			System.out.println("3. Mostrar una posició de la llista");
			System.out.println("4. Buscar si tenim un número a la llista i mostrar-lo.");
			System.out.println("5. Esborrar un número de la llista.");
			op = sc.nextInt();
			sc.nextLine();//borramos el salto de linea
			switch (op) {
			case 0:
				return;
			case 1:
				do {
					numero = sc.nextLine();
				}while(!numero.matches("[6-7][0-9]{8}"));
				//regex para comprobar que el primer digito sea 6 o 7 y las 8 siguientes sean numeros entre 0 y 9
				numeros.add(numero); //añadir un nuevo valor
				break;
			case 2:
				Collections.sort(numeros);//ordenar elementos
				System.out.println(numeros);
				break;
			case 3:
				op = sc.nextInt();
				sc.nextLine();
				if (op == 1) {
					System.out.println(numeros.get(sc.nextInt()));//devolver el objeto en el indice indicado				
				}else if(op == 2) {
					System.out.println(numeros.indexOf(sc.nextLine()));//devolver el indice del objeto indicado
				}
				break;
			case 4:
				numero = sc.nextLine();
				if (numeros.contains(numero)) {//verificamos si el elemento indicado existe
					System.out.println("Existe!");
				}else {
					System.out.println("No existe!");
				}
				break;
			case 5:
				numero = sc.nextLine();
				if (numeros.remove(numero)) {//borramos el elemento indicado y si lo hizo correctamente nos devuelve true
					System.out.println("Eliminado correctamente");
				}else {
					System.out.println("No existe!");
				}
				break;
			default:
				System.out.println("Opcion incorrecta");
				break;
			}
			System.out.println();
		} while (op != 0);

	}
}
