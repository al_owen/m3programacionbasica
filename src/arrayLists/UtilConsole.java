package arrayLists;

import java.util.Scanner;

public class UtilConsole {

	private static Scanner sc;
	
	public static void init() {
		sc = new Scanner(System.in);
	}
	
	public static void Close() {
		sc.close();
	}
	
	public static int pedirInt() {
		//int num = sc.nextInt();
		//sc.nextLine();
		String text;
		int num = 0;
		do {
			text = sc.nextLine();
			if (text.matches("[0-9]+")) {
				num = Integer.parseInt(text);
			}
		} while (!text.matches("[0-9]+") && num >= Integer.MAX_VALUE);
		
		return num;
	}
	
	public static String pedirString() {
		String text = sc.nextLine();
		return text;
	}
}
