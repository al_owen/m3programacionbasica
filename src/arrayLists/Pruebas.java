package arrayLists;

import java.util.ArrayList;
import java.util.Collections;

public class Pruebas {

	public static void main(String[] args) {
		ArrayList<String> listaCompra = new ArrayList<>();
		listaCompra.add("patata");//añadir un elemento nuevo
		listaCompra.add("pan");
		System.out.println(listaCompra.size());//ver el tamaño de la lista
		System.out.println(listaCompra);//imprimir la lista
		System.out.println(listaCompra.get(0));//obtener el objeto en el indice indicado
		listaCompra.set(1, "leche"); //remplazar el objeto en el indice indicado
		listaCompra.add(1, "leche"); //añadir un elemento en el indice indicado
		System.out.println(listaCompra);
		listaCompra.remove(1);//borrar el elemento en el indice indicado
		System.out.println(listaCompra);
		Object[] listaAcabada = listaCompra.toArray();//convertir a array la arraylist
		for (int i = 0; i < listaAcabada.length; i++) {
			System.out.print(listaAcabada[i]+" ");
		}
		System.out.println();
		System.out.println(listaCompra.indexOf("")); //ver el indice del elemento indicado
		Collections.sort(listaCompra);//ordenar la lista
		System.out.println(listaCompra);
		Collections.shuffle(listaCompra); //desordenar la lista
		System.out.println(listaCompra);
		Collections.reverse(listaCompra);//mostrar la lista invertida
		System.out.println(listaCompra);
	}
}
