package pokemon;

import java.util.ArrayList;
import java.util.Random;

public class Pokemon {

	public static int Creados;//es una variable statica, 
	//sirve para definir una variable a la que se puede 
	//acceder sin tener que crear un objeto
	
	//atributos del objeto
	/*Los atributos son variables que pertenecen al objeto,
	 * todos los objetos tendran los mismos atributos/variables, 
	 * pero cada uno tendrá un valor diferente
	 * */
	private String nombre;
	private ArrayList<String> tipos;
	private ArrayList<Ataque> ataques;
	private int nivel;
	private int hp, atck, def;
	
	//constructores del objeto
	/*el constructor es una especie de funcion a la que se llama
	 * en el momento de crear un objeto, puede pedir parametros(valores)
	 * o puede estar vacio
	 * */
	public Pokemon() {
		Creados++;
		Random r = new Random();
		nombre = "--";
		nivel = r.nextInt(1,101);
		hp = r.nextInt(1,101);
		atck = r.nextInt(1,101);
		def = r.nextInt(1,101);
	}
	/*en este caso el constructor pide valores que asigna a los atributos del objeto
	 * */
	public Pokemon(String name, int level, int hp, int atk, int def) {
		Creados++;
		nombre = name;
		nivel = level;
		this.hp = hp;
		this.atck = atk;
		this.def = def;
	}
	
	
	//funciones
	/*esta funcion recibe un valor y modifica el nombre 
	 * */
	protected void cambiarNombre(String nuevoNombre) {
		this.nombre = nuevoNombre;
	}
	//esta funcion devuelve el nombre del pokemon 
	public String mostrarNombre() {
		return this.nombre;
	}
	
	//esta funcion devuelve el nombre del pokemon 
	public String getNombre() {
		return this.nombre;
	}
	
}
