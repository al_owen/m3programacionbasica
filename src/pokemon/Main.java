package pokemon;

public class Main {

	public static void main(String[] args) {
		/*Creamos un pokemon pasandole valores
		 * estamos llamando al constructor que pide estos valores
		 * */
		Pokemon pokachu = new Pokemon("Pokachu",99, 50,87, 95);
		/*creamos un pokemon pero no le pasamos valores
		 * estamos llamando al constructor que no pide valores
		 * */
		Pokemon dikorita = new Pokemon();
		
		//creamos un pokemon, pero no lo guardamos en una variable
		new Pokemon();
		//mostramos la variable estatica
		//Para llamar a una variable static, no hace falta crear un objeto
		//Se llaman directamente llamando a la Clase.Variable
		System.out.println(Pokemon.Creados);
		//llamamos a la funcion getNombre() de pokachu
		//solo nos mostrará el nombre de pokachu
		System.out.println(pokachu.getNombre());
		//llamamos a la funcion getNombre() de dikorita
		//solo nos mostrará el nombre de dikorita
		System.out.println(dikorita.getNombre());
		
	}

}
