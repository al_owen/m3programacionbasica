package strings;

import java.util.Scanner;

public class Strings {

	public static void main(String[] args) {
		//String nom = new String("Álvaro");
		String nombre = "alvaro";
		String nombre2 = "alvaro";
		String apellido = "de la Quintana";
		String nombreApellido = nombre +" "+apellido;
		String nombre3 = nombreApellido.substring(0, 6)+nombreApellido.substring(12);
		//System.out.println(nombre2);
		System.out.println(nombre.charAt(0));
		System.out.println(nombreApellido.contains("de"));
		System.out.println(nombre.endsWith("o"));
		System.out.println(nombre.equals("Alvaro"));
		char[] array = nombre.toCharArray();
		System.out.println(array[2]);
		System.out.println(nombre.replaceAll("a", "e"));
		
		
		//String nombreApellido = "alvaro de la Quintana";
		//System.out.println(nombre+" "+apellido);
		
		StringBuilder name = new StringBuilder("Álvaro");
		name.append(" de la Quintana");
		name.delete(6, 12);
		//System.out.println(name);
	}
}
