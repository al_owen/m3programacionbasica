package strings;

import java.util.Scanner;

public class Exercici2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String temp = sc.nextLine();
		StringBuilder cosas = new StringBuilder(temp);
		cosas.reverse();
		System.out.println(cosas);
	}

}
