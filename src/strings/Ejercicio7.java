package strings;

import java.util.Scanner;

public class Ejercicio7 {

	public static void main(String[] arg) {
		Scanner sc = new Scanner(System.in);
		long num;
		do {
			num = sc.nextLong();
		} while (num < 0 || num > Integer.MAX_VALUE);
	}
}
