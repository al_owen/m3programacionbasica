package strings;

import java.util.Scanner;

public class Exercici1String {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String cosas = sc.nextLine();
		
		for (int i = cosas.length()-1; i >= 0 ; i--) {
			System.out.print(cosas.charAt(i));
		}
		
	}
}
