package strings;

import java.util.Iterator;
import java.util.Scanner;

public class Exercici3String {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String text = sc.nextLine();
		System.out.println(text.replaceAll("\\s+", " "));
		
		System.out.println("*********************");
		StringBuilder sb = new StringBuilder(text);
		
		int i = 0;
		while (i < sb.length()-1) {
			if (sb.charAt(i) == ' ' && sb.charAt(i+1) == ' ') {
				sb.deleteCharAt(i+1);
			}else {
				i++;
			}
		}
		System.out.println(sb);
		
	}
}
