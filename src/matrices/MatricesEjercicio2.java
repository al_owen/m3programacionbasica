package matrices;

public class MatricesEjercicio2 {
	public static void main(String[] args) {
		//crear matrices
		int[][] matA = {{2,0,1},{3,0,0},{5,1,1}};
		int[][] matB = {{1,0,1},{1,2,1},{1,1,0}};
		int[][] matRes = new int[matA.length][matB[0].length];
		
		//recorrer filas de matriz A
		for (int i = 0; i < matA.length; i++) {
			//recorrer columnas matriz B
			for (int j = 0; j < matB[i].length; j++) {
				//recorrer los elementos de la fila i columna actual para multiplicarlos
				for (int k = 0; k < matB[i].length; k++) {
					//matriz resultado = matrizA posicion i,k + MatrizB posicion k,j
					matRes[i][j] += matA[i][k]*matB[k][j];
					System.out.println(" posicion Matriz resultado "+i+" "+j+", "+(matA[i][k])+" * "+matB[k][j]+" = "+matRes[i][j]);
				}
			}
			
			
			
		}
	}
}
