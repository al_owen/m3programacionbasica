package matrices;

import java.util.Random;

public class MatricesEjercicio1 {

	public static void main(String[] args) {
		int[][] numeros = new int[3][3];
		Random r = new Random();
		
		for (int i = 0; i < numeros.length; i++) {
			for (int j = 0; j < numeros[i].length; j++) {
				numeros[i][j] = r.nextInt(1, 21);
			}
		}
		int max=0;
		for (int i = 0; i < numeros.length; i++) {
			for (int j = 0; j < numeros[i].length; j++) {
				System.out.print(numeros[i][j]+" ");
				if (max < numeros[i][j]) {
					max = numeros[i][j];
				}
			}
			System.out.println();
		}
		System.out.println("El numero más grande es: "+max);
	}
}
