package matrices;

import java.util.Iterator;

public class Matrices {
	public static void main(String[] args) {
		int[][] matrizRellena = {{1,2,3,5},{3,4,3,6},{5,6,4,5}};
		/*	[ [1,2,3,5],
		  [3,4,3,6],
		  [5,6,4,5] ]
				  */
		int[][] matriz = new int[15][15];
		int cont = 0;
		//llenar la matriz con valores
		for (int fila = 0; fila < matriz.length; fila++) {
			for (int col = 0; col < matriz[fila].length; col++) {
				matriz[fila][col] = cont;
				cont++;
			}
		}
		//imprimir la matriz
		for (int fila = 0; fila < matriz.length; fila++) {
			for (int col = 0; col < matriz[fila].length; col++) {
				System.out.print(matriz[fila][col]+" ");//imprimimos cada elemento
			}
			System.out.println();//hacemos un salto de linea cada vez que cambiemos de fila
		}
		System.out.println();
		matriz[1][2] = 24;//modificamos el valor de la posición de fila 1 columna 2
		
		for (int fila = 0; fila < matriz.length; fila++) {
			for (int col = 0; col < matriz[fila].length; col++) {
				System.out.print(matriz[fila][col]+" ");
			}
			System.out.println();
		}
	}
}
