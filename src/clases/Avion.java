package clases;

public class Avion {

	//variables
	public int maxAltitud;
	private int maxFuel;
	private int maxNrPasajeros;
	private double velocidad;
	private int nroMotores;
	
	//constructores
	public Avion(int maxAlt) {
		maxAltitud = maxAlt;
	}

	public Avion(int maxAlt, int maxFuel, int nroMotores) {
		super();
		maxAltitud = maxAlt;
		this.maxFuel = maxFuel;
		this.nroMotores = nroMotores;
	}

	public int getMaxFuel() {
		return maxFuel;
	}

	public void setMaxFuel(int maxFuel) {
		this.maxFuel = maxFuel;
	}

	public int getMaxNrPasajeros() {
		return maxNrPasajeros;
	}

	public void setMaxNrPasajeros(int maxNrPasajeros) {
		this.maxNrPasajeros = maxNrPasajeros;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public int getNroMotores() {
		return nroMotores;
	}

	public void setMaxAltitud(int maxAltitud) {
		if (maxAltitud < 1500) {
			this.maxAltitud = maxAltitud;			
		}
	}
	
	//funciones
	
}
